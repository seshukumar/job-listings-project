import { Pipe, PipeTransform,  } from '@angular/core';
import { DatePipe } from '@angular/common';


@Pipe({
  name: 'transformDate'
})
export class TransformDatePipe implements PipeTransform {

  constructor(public datePipe: DatePipe){ }

  transform(value: any): any {
    const date = Date.parse(value);
    if (isNaN(date)) {
      return '';
    } else {
      return this.datePipe.transform(value);
    }
  }
}
