import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { JobDetailsCardComponent } from './job-details-card/job-details-card.component';
import { TransformDatePipe } from './transform-date/transform-date.pipe';

import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [SearchComponent, JobDetailsCardComponent, TransformDatePipe],
  imports: [ CommonModule, FormsModule ],
  providers: [DatePipe],
  exports: [SearchComponent, JobDetailsCardComponent]
})
export class SharedModule { }
