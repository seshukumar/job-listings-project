import { Component, Output, EventEmitter } from '@angular/core';
import { ISearchModel } from 'src/app/core/search.interface';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  @Output() searchEvent = new EventEmitter();

  searchModel: ISearchModel = {
    skills: undefined,
    locations: undefined,
    experience: undefined
  };

  search() {
    this.searchEvent.emit(JSON.stringify(this.searchModel));
  }

  // reset the experience value to 0 if the user enters negative value
  resetExperience() {
    if (this.searchModel.experience < 0) {
      this.searchModel.experience = 0;
    }
  }
}
