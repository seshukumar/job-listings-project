import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { IJob } from 'src/app/core/job.interface';


@Component({
  selector: 'app-job-details-card',
  templateUrl: './job-details-card.component.html',
  styleUrls: ['./job-details-card.component.css']
})
export class JobDetailsCardComponent {

  @Input() job: IJob;
  @Input() fullContent = false;
  @Output() openModal = new EventEmitter();

  readMore() {
    event.preventDefault();
    this.openModal.emit(this.job._id);
  }

}
