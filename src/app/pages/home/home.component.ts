import { Component, OnInit } from '@angular/core';
import { JobListingsService } from '../services/job-listings.service';
import { IJob } from 'src/app/core/job.interface';
import { map } from 'rxjs/operators';
import { ISearchModel } from 'src/app/core/search.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  jobsList: Array<IJob>;
  filteredJobsList: Array<IJob>;
  searchModel: ISearchModel;

  showModal = false;
  // number of items to display by default
  limit = 20;
  selectedJobIndex: number;


  constructor(private jobsService: JobListingsService) {}

  ngOnInit() {
    /*
      get the job details array and assign to jobList and filteredList.
      filteredList array holds the filtered items based on the search model
      by default it holds all jobList list array.
      jobsList array holds original array which is used to reset the filteredList array
      every time the search model is cleared, with out making a round trip to the server.
    */

    this.jobsService.getJobListings().subscribe((result: Array<IJob>) => {
      this.jobsList = result;
      this.filteredJobsList = result;
    });
  }

  search(model: string) {
    this.searchModel = JSON.parse(model) as ISearchModel;
    this.filterJobs();
  }

  // display more items in page
  viewMore() {
    this.limit += 20;
  }

  openModal(id) {
    this.selectedJobIndex = this.filteredJobsList.findIndex(job => job._id === id);
    if (this.selectedJobIndex > -1) {
      this.showModal = true;
    }
    console.log('selected job index', this.selectedJobIndex);
    console.log('filteredJobList', this.filteredJobsList);
  }

  closeModal() {
    this.showModal = false;
    this.selectedJobIndex = undefined;
  }

  filterJobs() {
    // if the user clicks the search button without entering anything assign the filtered list with jobListing array
    if (
      !this.searchModel.skills &&
      !this.searchModel.locations &&
      !this.searchModel.experience
    ) {
      return (this.filteredJobsList = this.jobsList);
    }

    let skills: Array<string>;
    let locations: Array<string>;
    let experience: number;

    // separate multiple skills by ','
    if (this.searchModel.skills) {
      skills = this.searchModel.skills.toLowerCase().split(',');
    }

    // separate multiple locations by ','
    if (this.searchModel.locations) {
      locations = this.searchModel.locations.toLowerCase().split(',');
    }

    if (this.searchModel.experience) {
      experience = this.searchModel.experience;
    }

    this.filteredJobsList = this.jobsList.filter(job => {
      let includeSkills = true;
      let includeLocations = true;
      let includeExperience = true;

      if (skills) {
        includeSkills = skills.some(
          str => job.skills.toLowerCase().indexOf(str.trim()) > -1
        );
      }

      if (locations) {
        includeLocations = locations.some(
          str => job.location.toLowerCase().indexOf(str.trim()) > -1
        );
      }

      // get the experience years by splitting the string twice
      if (experience) {
        let requiredExperience = job.experience.split(' ');

        if (requiredExperience.length > 0) {
          requiredExperience = requiredExperience[0].split('-');
          includeExperience =
            experience >= +requiredExperience[0] &&
            experience <= +requiredExperience[1];
        } else {
          if (requiredExperience[0] === 'Fresher') {
            requiredExperience[0] = '0';
          }
          includeExperience = experience >= +requiredExperience[0];
        }
      }

      return includeSkills && includeLocations && includeExperience;
    });
  }
}
