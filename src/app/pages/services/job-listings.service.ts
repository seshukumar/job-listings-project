import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IJob } from 'src/app/core/job.interface';
import { map } from 'rxjs/operators';

@Injectable()
export class JobListingsService {

  constructor(private http: HttpClient) {}

  getJobListings(): Observable<Array<IJob>> {
    return this.http.get('https://nut-case.s3.amazonaws.com/jobs.json')
      .pipe(
        map((result: { data: Array<IJob> }) => result.data)
      );
  }
}
