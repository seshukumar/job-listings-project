export interface ISearchModel {
  skills: string;
  locations: string;
  experience: number;
}
